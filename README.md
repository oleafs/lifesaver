# Lifesaver #

Repositori ini adalah kumpulan ide-ide yang mampu memudahkan kehidupan developer sehari-hari. Silahkan menikmati

## Usap ##

### Tentang Usap ###

Usap adalah sebuah skrip yang ditulis dalam shell script bash. Usap ditujukan untuk membantu developer atau sistem administrator yang sering melakukan kegiatan remote server via SSH. 

Agar proses pengaksesan ssh lebih cepat dan mudah usap akan membantu developer dan sysadmin dengan membuatkan profile serta memasukan ssh public key ke server yang akan digunakan. Sehinga pengguna tidak perlu repot-repot memasukan password setiap kali akan memulai sesi SSH.